# wtfi

A package for productive coding in Python

## Installation

```bash
$ pip install wtfi
```

## Setup

To stay productive and avoid procrastination install [Geckodriver](https://github.com/mozilla/geckodriver/releases) for Firefox and add it to the system path. It lets WTFi control the time you spend on a break. 

You should set up a profile for your browser that stores the credentials for the social media. Follow these [instructions](https://support.mozilla.org/en-US/kb/profile-manager-create-remove-switch-firefox-profiles#w_creating-a-profile), and then in a new tab log in to WhatsApp, TikTok and Instagram using the created profile. 

## Usage

```bash
$ wtfi.do()
```
 Opens a promt to put in the path to your Firefox profile, number of working blocks your are planning to do, work and pause interval duration. During the break you can relax on WhatsApp, TikTok or Instagram pages. 

## Contributing

Interested in contributing? Check out the contributing guidelines. Please note that this project is released with a Code of Conduct. By contributing to this project, you agree to abide by its terms.

## License

`wtfi` was created by Dima & Tugce. It is licensed under the terms of the MIT license.

## Credits

`wtfi` was created with [`cookiecutter`](https://cookiecutter.readthedocs.io/en/latest/) and the `py-pkgs-cookiecutter` [template](https://github.com/py-pkgs/py-pkgs-cookiecutter).
