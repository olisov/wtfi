#Import global packages
import time
import re
from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FirefoxOptions


#User input to define work, snooze and pause durations

class intervall:

    def __init__(self, UserInput):
   
        self.validate(UserInput)
        
        if self.validate(UserInput) == True:
            self.value(UserInput)
        else:
            self.minutes = 0
            self.seconds = 0
            print("Please write a valid time (mm:ss)")
   
    def validate(self, UserInput):
        
        pattern = r'^\d:(?:[0-5]?[0-9])$'
        
        if re.match(pattern, UserInput):
            return True
        else:     
            return False
       
    def value(self, UserInput):
        
        self.minutes : int= [int(n) for n in UserInput.split(":")][0]
        self.seconds : int= self.minutes * 60 + [int(n) for n in UserInput.split(":")][1]



def launch_wtfi(seconds: int=300, profile_path: str = ''):
    
    # Initialize the driver
    ops = FirefoxOptions()
    ops.add_argument(f"-profile={profile_path}")
    driver = webdriver.Firefox(options=ops)
    
    # URLs to open
    urls = ['https://web.whatsapp.com/', 'https://www.instagram.com/', 'https://www.tiktok.com/']

    # Open each URL in a new tab
    for url in urls:
        driver.execute_script(f"window.open('{url}');")

    # Wait for the specified time
    time.sleep(seconds)

    # Close the browser
    driver.quit()

def do():
    
    invalid_path = True

    while invalid_path == True :
        profile_path = str(input("Whats your profile path?"))
        
        if len(profile_path) == 0:
            invalid_path = True

        elif len(profile_path) > 0:
            invalid_path = False

    invalid_total = True
            
    while invalid_total == True :
        
        try:
            blocks =  int(input("How many blocks of work do you want to have today?"))
        
        except ValueError:
            blocks = 0
            
        if blocks > 0:
            invalid_total = False
            
        else:
            invalid_total = True
            print("Please provide a valid number")
    
    invalid_work = True

    
    invalid_work = True
            
    while invalid_work == True :
        work_int = intervall(input("How long do you want to one block of work to be? (mm:ss):"))
        
        if work_int.seconds == 0:
            invalid_work = True
            
        elif work_int.seconds > 0:
            invalid_work = False
            #print(work_int.seconds)
    
    invalid_pause = True
            
    while invalid_pause == True :
        pause_int = intervall(input("How long do you want a break to be? (mm:ss):"))
        
        if pause_int.seconds == 0:
            invalid_pause = True
            
        elif pause_int.seconds > 0:
            invalid_pause = False
            #print(pause_int.seconds)
    
    
    # Start the work block timer
    
    for b in range(0, blocks):
    
        # Work intervall
        print("New work block starting")
        
        time.sleep(work_int.seconds)
        
        # Pause starts
            
        yes = {"yes","y", "Yes", "Y"}
        no = {"no","n", "No", "N"}
        
        invalid_choice = True
        pause = True
        
        while invalid_choice == True and b < blocks-1:
            
            choice = input("Do you want to take a break? [y/n]?")
                
            if choice in yes:
                invalid_choice = False
                pause = True
                           
            elif choice in no:
                invalid_choice = False
                pause = False
                    
        # Pause starts
    
        if invalid_choice == False and b < blocks-1:
            
            if pause == True:
                print("Enjoy your break")
                
                launch_wtfi(pause_int.seconds, profile_path)
                
        # No pause
    
            if pause == False:
                print ("No break")
            
    print ("You completed the number of work blocks you aimed for, good boi")


if __name__ == "__main__":
    do()
